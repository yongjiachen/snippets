#!/bin/bash
set -e

mkdir -p data

if [ -n "$1" ]; then
    exec "$@"
else
    ./app
fi